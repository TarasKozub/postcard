<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Content area -->
            <div class="content">
                <!-- Simple panel -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">My profile</h5>
                    </div>
                <div class="row panel-body">
                    <div class="col-md-3 col-md-offset-1">
                        <div class="thumbnail">
                            <div class="thumb">
                                <img src="images/placeholder.jpg" alt="">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <!-- Basic layout-->
                        <form action="#" class="form-horizontal">
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Name:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" placeholder="Eugene Kopyov">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Email:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" placeholder="user@google.com">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Birthday:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" placeholder="01/01/2000">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Password:</label>
                                        <div class="col-lg-9">
                                            <input type="password" class="form-control" placeholder="Your strong password">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Gender:</label>
                                        <div class="col-lg-9">
                                            <label class="radio-inline">
                                                <input type="radio" class="styled" name="gender" checked="checked">
                                                Male
                                            </label>

                                            <label class="radio-inline">
                                                <input type="radio" class="styled" name="gender">
                                                Female
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Your avatar:</label>
                                        <div class="col-lg-9">
                                            <input type="file" class="file-styled">
                                            <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary">Save Changes<i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- /basic layout -->
                    </div>
                </div>

                </div>
                <!-- /simple panel -->
            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->