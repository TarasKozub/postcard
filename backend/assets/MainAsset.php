<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css',
        'css/icons/icomoon/styles.css',
        'css/bootstrap.css',
        'css/core.css',
        'css/components.css',
        'css/colors.css',
    ];
    public $js = [
        'js/plugins/loaders/pace.min.js',
        'js/core/libraries/jquery.min.js',
        'js/core/libraries/bootstrap.min.js',
        'js/plugins/loaders/blockui.min.js',
        'js/plugins/forms/inputs/typeahead/handlebars.min.js',
        'js/plugins/forms/inputs/alpaca/alpaca.min.js',
        'js/plugins/forms/inputs/alpaca/price_format.min.js',
        'js/plugins/forms/selects/select2.min.js',
        'js/plugins/ui/prism.min.js',
        'js/plugins/forms/styling/uniform.min.js',
        'js/core/app.js',
        'js/pages/alpaca_advanced.js',
    ];
    public $depends = [
    ];
}